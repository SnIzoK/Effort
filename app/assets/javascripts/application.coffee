#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global

#     P L U G I N S

#= require plugins/jquery-easing
#= require plugins/clickout
#= require plugins/form
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
#= require plugins/slick.min
#= require plugins/slick-init
#= require plugins/jquery.bxslider
#= require plugins/bx


#     I N I T I A L I Z E

#= require google_map
#= require header
#= require menu
#= require accordion
#= require popups
#= require tabs
#= require links