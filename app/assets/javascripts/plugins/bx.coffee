$(document).ready ->

  banner_slider = $('.discounts-slider').bxSlider
    controls: false
    pager: false
    speed: 1000
    pause: 5000
    # mode: 'fade'
    # auto: true
    onSliderLoad: (currentIndex)->
    onSlideBefore: ($slideElement, oldIndex, newIndex)->
      $('.current-slide').text(banner_slider.getCurrentSlide()+1)
  if banner_slider.getSlideCount
    $('.total-slide').text(banner_slider.getSlideCount())
  $('.slider-button-box .prev-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToPrevSlide()

  $('.slider-button-box .next-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToNextSlide()

